package lt.saltyjuice.dragas.camel.tracker.core;

public class Assignee implements Comparable<Assignee> {
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int compareTo(Assignee o) {
        return (int) (getId() - o.getId());
    }
}
