package lt.saltyjuice.dragas.camel.tracker.core;

import java.time.Instant;
import java.util.Set;
import java.util.TreeSet;

public class Task implements Comparable<Task> {
    private long id;
    private Instant startedAt;
    private Instant endedAt;
    private String name = "";
    private Set<Task> subtasks;
    private Assignee assignee = new Assignee();
    private Task parent; // to prevent circles in graph

    public Task() {
        subtasks = new TreeSet<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        if (this.id == 0)
            this.id = id;
    }

    private Task getParent() {
        return parent;
    }

    private void setParent(Task task) {
        this.parent = task;
    }

    public Instant getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Instant startedAt) {
        this.startedAt = startedAt;
    }

    public Instant getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Instant endedAt) {
        this.endedAt = endedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Task> getSubtasks() {
        return subtasks;
    }

    public void setSubtasks(Set<Task> subtasks) {
        this.subtasks = subtasks;
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    public boolean isFinished() {
        return startedAt != null && endedAt != null && isValid() && subtasks.stream().allMatch(Task::isFinished);
    }

    private boolean isValid() {
        return startedAt.isBefore(endedAt);
    }

    public boolean addSubtask(Task task) {
        if (task.getParent() == null) {// to prevent circles in graph
            task.setParent(this);
            return subtasks.add(task); // should it throw otherwise?
        }
        return false;
    }

    public boolean removeSubtask(Task task) {
        if (subtasks.remove(task)) {
            task.setParent(null);
            task.subtasks.forEach((it) -> it.setParent(null));
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Task o) {
        int finishedComparison = Boolean.compare(isFinished(), o.isFinished());
        if (finishedComparison != 0)
            return finishedComparison;
        return getName().compareTo(o.getName());
    }
}
