package lt.saltyjuice.dragas.camel.tracker.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class TaskTest {
    private Task task;

    @BeforeEach
    public void initialize() {
        task = getFinishedTask();
    }

    @Test
    public void itShouldBeFinishedWhenEndIsAfterStart() {
        assertTrue(task.isFinished());
    }

    @Test
    public void itShouldNotBeFinishedWhenEndIsBeforeStart() {

        task.setStartedAt(task.getStartedAt().plusSeconds(2));
        assertFalse(task.isFinished());
    }

    @Test
    public void itShouldNotBeFinishedWithNullStart() {

        task.setStartedAt(null);
        assertFalse(task.isFinished());
    }

    @Test
    public void itShouldNotBeFinishedWithNullEnd() {

        task.setEndedAt(null);
        assertFalse(task.isFinished());
    }

    @Test
    public void itShouldBeFinishedWithFinishedChildren() {
        task.addSubtask(getFinishedTask());
        assertTrue(task.isFinished());
    }

    @Test
    public void itShouldNotBeFinishedWithUnfinishedChildren() {
        Task task = getFinishedTask();
        task.setEndedAt(null);
        this.task.addSubtask(task);
        assertFalse(this.task.isFinished());
    }

    private Task getFinishedTask() {
        Task task = new Task();
        task.setStartedAt(Instant.now());
        task.setEndedAt(Instant.now().plusSeconds(1)); // since two consecutive instant.now calls result in "same"
        task.setName("");
        return task;// instant, adding a second is necessary
    }
}
