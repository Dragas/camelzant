# Camel Tracker

Apache camel task list application wrapped into spring boot

## Running

If you're running on linux, you first will need to
make `mvnw` executable.

```bash
chmod +x mvnw
```

Afterwards invoke the following to start the springboot instance:

```bash
./mvnw clean install
./mvnw spring-boot:run -f ./camel-boot/pom.xml
```

To run tests, you may call the `test` goal like so:

```bash
./mvnw test
```

Mind you only `tracker-core` module has general sanity tests.

## Usage

Before client can do anything, they must make a `GET` request to `/events`, to obtain
a cookie that is used to control which event stream they're using. Client MUST not lose
reference to that connection, otherwise they will not be notified of any changes.

Each request, except for `/events` MUST have an `X-Request-Nonce` custom header that
contains an arbitrary value, so that the client would be notified about that particular change in application during eventual completion. In addition, each request requires a `cookie-id` cookie so camel would know which event stream to respond to. 
As a result, each request responds with `202 Accepted` as a confirmation that the request got put into camel's context.

Once request is completed, `EventStream` responds in following format:

```
name:response
reconnect:30000
data:ResponseObject<T>
```

Where `ResponseObject<T>` is JSON with the following format:

```json
{
  "nonce": string,
  "tracker-body": T
}
```

`T` can be one of `Task, long, boolean, string` depending on request that was made.

## Routes

### GET - /task/{id}

Requires:
```
PathParam: id : long
```

Returns `ResponseObject<Task>`, where Task is defined as follows:

```json
{
  id: long,
  startedAt: long | null,
  endedAt: long | null,
  name: string,
  assignee: { id: long },
  subtasks: [Task],
  parent: Task
}
```

### POST - /task

Requires: Nothing in addition

Returns a `ReponseObject<Long>` object, where the value corresponds to new task's id.

### PATCH - /task/{id}?key={key}&value={value}

Requires:
```
PathParam: id : long
key: one of [NAME, START, END, ASSIGNEE]
value: depending on key, it can correspond to one of: [string, long (miliseconds), long (miliseconds), long (id)
```

Returns either a `ResponseObject<String>` or `ResponseObject<Long>` with the new value for the updated field as a confirmation that it succeeded. 

### DELETE - /task/{id}

Requires:
```
PathParam: id : long
```

Deletes task with provided id but not its children, so they could be reattached to something else. Returns a `ResponseObject<Long>` with the value corresponding to deleted element's id.

### PATCH - /task/{id}/attach?subtask={subtaskid}

Requires:
```
PathParam: id : long
subtaskid: id : long
```

Attaches subtask with provided id of `subtaskid` to a task with id of `id`. Returns `ResponseObject<Boolean>` with value that corresponds if the attachment suceeded.

### PATCH - /task/{id}/detach?subtask={subtaskid}

Same as `attach` but returns a value if detachment succeeded.  
