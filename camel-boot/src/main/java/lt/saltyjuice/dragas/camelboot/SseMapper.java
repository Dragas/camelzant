package lt.saltyjuice.dragas.camelboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

@Component
public class SseMapper {

    private static final Logger LOG = LoggerFactory.getLogger(SseMapper.class);

    private final Map<String, SseEmitter> cookieEmitters = Collections.synchronizedMap(new TreeMap<>());

    public SseEmitter getEmitterFor(String cookie) {
        return cookieEmitters.computeIfAbsent(cookie, this::createEmitter);
    }

    private SseEmitter createEmitter(String cookie) {
        SseEmitter emitter = new SseEmitter(TimeUnit.HOURS.toMillis(1));
        emitter.onCompletion(() -> {
            remove(cookie);
        });
        emitter.onError((it) -> {
            remove(cookie);
            LOG.error("Something happened", it);
        });
        return emitter;
    }

    public void remove(String cookie) {
        SseEmitter emitter = cookieEmitters.remove(cookie);
        if(emitter != null)
            emitter.complete(); // repeated invocations do nothing so its cool
    }

    public void broadcast(SseEmitter.SseEventBuilder builder) {
        Map<String, SseEmitter> cloned = new TreeMap<>(cookieEmitters);
        cloned.forEach((key, value) -> {
            try {
                value.send(builder);
            } catch (IOException e) {
                remove(key);
                LOG.error("Something happened", e);
            }
        });
    }

    @PreDestroy
    public synchronized void cleanup() {
        Map<String, SseEmitter> cloned = new TreeMap<>(cookieEmitters);
        cloned.forEach((key, value) -> value.complete());
    }
}
