package lt.saltyjuice.dragas.camelboot;

import lt.saltyjuice.dragas.camel.TaskRouteBuilder;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.annotation.PreDestroy;
import javax.websocket.server.PathParam;
import java.util.HashMap;
import java.util.Map;

@RestController
public class TaskRestController {

    public static final String X_REQUEST_NONCE = "X-Request-Nonce";
    public static final String ID = "id";
    public static final String CLIENT_ID = "client-id";
    private final ProducerTemplate template;
    private final SseMapper sseMapper;

    public TaskRestController(CamelContext context, SseMapper mapper) throws Exception {
        this.template = context.createProducerTemplate();
        sseMapper = mapper;
    }

    @RequestMapping(path = "/task/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void getTask(@PathVariable(ID) long id, @RequestHeader(X_REQUEST_NONCE) String nonce, @CookieValue(CLIENT_ID) String cookie) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(TaskRouteBuilder.NONCE, nonce);
        template.sendBodyAndHeaders(TaskRouteBuilder.TASK_GET, id, headers);
    }

    @RequestMapping(path = "/task", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void createTask(@RequestHeader(X_REQUEST_NONCE) String nonce, @CookieValue(CLIENT_ID) String cookie) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(TaskRouteBuilder.NONCE, nonce);
        template.sendBodyAndHeaders(TaskRouteBuilder.TASK_CREATE, null, headers);
    }

    @RequestMapping(path = "/task/{id}", method = RequestMethod.PATCH)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateTask(@RequestHeader(X_REQUEST_NONCE) String nonce, @CookieValue(CLIENT_ID) String cookie, @PathParam(ID) long id, @RequestParam("key") String key, @RequestParam("value") String value) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(TaskRouteBuilder.NONCE, nonce);
        headers.put(TaskRouteBuilder.TASK_ID, id);
        headers.put(TaskRouteBuilder.TASK_FIELD, key);
        template.sendBodyAndHeaders(TaskRouteBuilder.TASK_UPDATE, value, headers);
    }

    @RequestMapping(path = "/task/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteTask(@RequestHeader(X_REQUEST_NONCE) String nonce, @CookieValue("cookie-id") String cookie, @PathParam(ID) long id) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(TaskRouteBuilder.NONCE, nonce);
        template.sendBodyAndHeaders(TaskRouteBuilder.TASK_DELETE, id, headers);
    }

    @RequestMapping(path = "/task/{id}/attach", method = RequestMethod.PATCH)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void attachTask(@RequestHeader(X_REQUEST_NONCE) String nonce, @CookieValue("cookie-id") String cookie, @PathParam(ID) long id, @RequestParam("subtask") long subtaskId) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(TaskRouteBuilder.NONCE, nonce);
        headers.put(TaskRouteBuilder.TASK_ID, id);
        template.sendBodyAndHeaders(TaskRouteBuilder.TASK_ATTACH, subtaskId, headers);
    }

    @RequestMapping(path = "/task/{id}/detach", method = RequestMethod.PATCH)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void detachTask(@RequestHeader(X_REQUEST_NONCE) String nonce, @CookieValue("cookie-id") String cookie, @PathParam(ID) long id, @RequestParam("subtask") long subtaskId) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(TaskRouteBuilder.NONCE, nonce);
        headers.put(TaskRouteBuilder.TASK_ID, id);
        template.sendBodyAndHeaders(TaskRouteBuilder.TASK_DETACH, subtaskId, headers);
    }

    @RequestMapping(path = "/events", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SseEmitter> events(@CookieValue(value = CLIENT_ID, required = false) String cookie) {
        if (cookie == null || cookie.equals(ValueConstants.DEFAULT_NONE))
            cookie = Long.toString((long)(Math.random() * Long.MAX_VALUE), Character.MAX_RADIX);
        SseEmitter emitter = sseMapper.getEmitterFor(cookie);
        //response.addCookie(new Cookie(CLIENT_ID, cookie).setDomain("localhost:8081"));
        return ResponseEntity
                .ok()
                .header(HttpHeaders.SET_COOKIE, ResponseCookie.from(CLIENT_ID, cookie).httpOnly(true).build().toString())
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(emitter);
    }

    @PreDestroy
    public void cleanup() throws Exception {
        template.stop();
    }
}
