package lt.saltyjuice.dragas.camelboot;

import lt.saltyjuice.dragas.camel.TaskRouteBuilder;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class SseResponder implements Processor {

    private final SseMapper sseMapper;

    public SseResponder(SseMapper sseMapper) {
        this.sseMapper = sseMapper;
    }

    /**
     * Processes the message exchange
     *
     * @param exchange the message exchange
     * @throws Exception if an internal processing error has occurred.
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, Object> response = new HashMap<>();
        response.put(TaskRouteBuilder.NONCE, exchange.getIn().getHeader(TaskRouteBuilder.NONCE, String.class));
        response.put(TaskRouteBuilder.BODY, exchange.getIn().getBody());
        SseEmitter.SseEventBuilder event = SseEmitter.event().name("response").data(response).reconnectTime(TimeUnit.SECONDS.toMillis(30));
        sseMapper.broadcast(event);
    }
}
