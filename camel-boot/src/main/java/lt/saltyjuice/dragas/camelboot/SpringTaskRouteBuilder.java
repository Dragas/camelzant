package lt.saltyjuice.dragas.camelboot;

import lt.saltyjuice.dragas.camel.TaskRouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class SpringTaskRouteBuilder extends TaskRouteBuilder {

    private final SseResponder sseResponder;

    public SpringTaskRouteBuilder(SseResponder sseResponder) {
        this.sseResponder = sseResponder;
    }

    @Override
    public void configure() throws Exception {
        super.configure();
        from(TASK_RESPOND)
                .validate((it) -> it.getIn().getHeader(NONCE, String.class) != null)
                .validate((it) -> it.getIn().getBody() != null)
                .process(this.sseResponder);
    }
}
