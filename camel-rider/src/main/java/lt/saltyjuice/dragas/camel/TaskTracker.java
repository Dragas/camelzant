package lt.saltyjuice.dragas.camel;

import lt.saltyjuice.dragas.camel.tracker.core.Task;

import java.util.NoSuchElementException;
import java.util.TreeMap;

public class TaskTracker {

    private final TreeMap<Long, Task> tracker = new TreeMap<>();

    public long createTask() {
        long maximum = 0;

        synchronized (tracker) {
            try {
                maximum = tracker.lastKey();
            } catch (NoSuchElementException err) {
                // map is empty so
                // we can safely assume
                // that new maximum can be 1
            }
            maximum++;
            Task task = new Task();
            task.setId(maximum);
            tracker.put(maximum, task);
        }
        return maximum;
    }

    public void deleteTask(long id) {
        synchronized (tracker) {
            tracker.remove(id);
        }
    }

    public Task getTask(long id) {
        return tracker.get(id);
    }
}
