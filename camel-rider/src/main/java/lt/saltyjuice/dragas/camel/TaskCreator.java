package lt.saltyjuice.dragas.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class TaskCreator implements Processor {

    private final TaskTracker tracker;

    public TaskCreator(TaskTracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        long id = tracker.createTask();
        exchange.getOut().setBody(id);
        exchange.getOut().setHeader(TaskRouteBuilder.NONCE, exchange.getIn().getHeader(TaskRouteBuilder.NONCE));
    }
}
