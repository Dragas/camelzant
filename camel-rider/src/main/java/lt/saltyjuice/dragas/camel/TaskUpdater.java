package lt.saltyjuice.dragas.camel;

import lt.saltyjuice.dragas.camel.tracker.core.Task;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.time.Instant;

public class TaskUpdater implements Processor {
    private final TaskTracker tracker;

    public TaskUpdater(TaskTracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        long taskId = exchange.getIn().getHeader(TaskRouteBuilder.TASK_ID, Long.class);
        Task task = tracker.getTask(taskId);
        Update method = Update.valueOf(exchange.getIn().getHeader(TaskRouteBuilder.TASK_FIELD, String.class));
        Object body = exchange.getIn().getBody();
        String nonce = exchange.getIn().getHeader(TaskRouteBuilder.NONCE, "", String.class);
        method.update(task, body);
        exchange.getOut().setBody(body); // i guess return the new value
        exchange.getOut().setHeader(TaskRouteBuilder.NONCE, nonce); // to make sure the client knows what he got response for
    }

    public enum Update {
        NAME() {
            @Override
            public void update(Task task, Object value) {
                String name = (String) value;
                task.setName(name);
            }
        },
        START() {
            @Override
            public void update(Task task, Object value) {
                Instant instant = (Instant) value;
                task.setStartedAt(instant);
            }
        },
        END() {
            @Override
            public void update(Task task, Object value) {
                Instant instant = (Instant) value;
                task.setEndedAt(instant);
            }
        },
        ASSIGNEE() {
            @Override
            public void update(Task task, Object value) {
                long id = (long) value;
                task.getAssignee().setId(id);
            }
        };

        public abstract void update(Task task, Object value);
    }
}
