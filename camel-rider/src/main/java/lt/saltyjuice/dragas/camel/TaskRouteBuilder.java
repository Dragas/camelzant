package lt.saltyjuice.dragas.camel;

import org.apache.camel.builder.RouteBuilder;

public class TaskRouteBuilder extends RouteBuilder {
    public static final String TASK_ID = "tracker-id";
    public static final String TASK_FIELD = "tracker-update-method";
    public static final String NONCE = "tracker-nonce";
    public static final String TASK_CREATE = "direct:task:create";
    public static final String TASK_RESPOND = "direct:task:respond";
    public static final String TASK_DELETE = "direct:task:delete";
    public static final String TASK_DETACH = "direct:task:detach";
    public static final String TASK_ATTACH = "direct:task:attach";
    public static final String TASK_UPDATE = "direct:task:update";
    public static final String TASK_GET = "direct:task:get";
    public static final String BODY = "tracker-body";
    private final TaskTracker tracker = new TaskTracker();

    @Override
    public void configure() throws Exception {
        // probably could use recipient list to refactor
        // all that validation into single route
        from(TASK_CREATE)
                .validate((it) -> it.getIn().getHeader(NONCE) != null)// can i refactor these into method references?
                .process(new TaskCreator(tracker))
                .to(TASK_RESPOND);
        from(TASK_UPDATE)
                .validate((it) -> it.getIn().getHeader(NONCE) != null)// can i refactor these into method references?
                .validate((it) -> it.getIn().getHeader(TASK_ID, 0, Long.class) > 0)
                .validate((it) -> TaskUpdater.Update.valueOf(it.getIn().getHeader(TASK_FIELD, String.class)) != null)// either will return true or throw
                .process(new TaskUpdater(tracker))
                .to(TASK_RESPOND);
        from(TASK_DELETE)
                .validate((it) -> it.getIn().getHeader(NONCE) != null)
                .validate((it) -> it.getIn().getBody(Long.class) > 0)
                .process(new TaskDeleter(tracker))
                .to(TASK_RESPOND);
        from(TASK_ATTACH)
                .validate((it) -> it.getIn().getHeader(NONCE) != null)// can i refactor these into method references?
                .validate((it) -> it.getIn().getHeader(TASK_ID, 0, Long.class) > 0)
                .validate((it) -> it.getIn().getBody(Long.class) > it.getIn().getHeader(TASK_ID, Long.class))
                .process(new TaskAttacher(tracker))
                .to(TASK_RESPOND);
        from(TASK_DETACH)
                .validate((it) -> it.getIn().getHeader(NONCE) != null)
                .validate((it) -> it.getIn().getHeader(TASK_ID, 0, Long.class) > 0)
                .validate((it) -> it.getIn().getBody(Long.class) > it.getIn().getHeader(TASK_ID, Long.class))
                .process(new TaskDetacher(tracker))
                .to(TASK_DELETE);

        from(TASK_GET)
                .validate((it) -> it.getIn().getHeader(NONCE) != null)
                .validate((it) -> it.getIn().getBody(Long.class) > 0)
                .process(new TaskGetter(tracker))
                .to(TASK_RESPOND);
    }
}
