package lt.saltyjuice.dragas.camel;

import lt.saltyjuice.dragas.camel.tracker.core.Task;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class TaskGetter implements Processor {
    private final TaskTracker tracker;

    public TaskGetter(TaskTracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        String nonce = exchange.getIn().getHeader(TaskRouteBuilder.NONCE, String.class);
        long id = exchange.getIn().getBody(Long.class);
        Task task = tracker.getTask(id);
        exchange.getOut().setHeader(TaskRouteBuilder.NONCE, nonce);
        exchange.getOut().setBody(task);
    }
}
