package lt.saltyjuice.dragas.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class TaskDeleter implements Processor {
    private final TaskTracker tracker;

    public TaskDeleter(TaskTracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        long id = exchange.getIn().getBody(Long.class);
        tracker.deleteTask(id);
        exchange.getOut().setBody(id);
        exchange.getOut().setHeader(TaskRouteBuilder.NONCE, exchange.getIn().getHeader(TaskRouteBuilder.NONCE));
    }
}
