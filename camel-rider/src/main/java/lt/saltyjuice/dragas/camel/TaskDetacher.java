package lt.saltyjuice.dragas.camel;

import lt.saltyjuice.dragas.camel.tracker.core.Task;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class TaskDetacher implements Processor {
    private final TaskTracker tracker;

    public TaskDetacher(TaskTracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        long id = exchange.getIn().getHeader(TaskRouteBuilder.TASK_ID, Long.class);
        long subtaskId = exchange.getIn().getBody(Long.class);
        Task task = tracker.getTask(id);
        Task subtask = tracker.getTask(subtaskId);
        boolean result = task.removeSubtask(subtask);
        if (result) { // send data only if the subtask was in fact a subtask
            exchange.getOut().setBody(subtaskId);
            exchange.getOut().setHeader(TaskRouteBuilder.NONCE, exchange.getIn().getHeader(TaskRouteBuilder.NONCE, String.class));
        }
    }
}
